# GenTwitch
This module give you the possibility to generate views
on your streamed videos

# Installation
You need python 3 from python.org

You need colorama (included in the repo), extract and install it through the console: 
```shell
cd colorama-x.x.x.tar.gz
python setup.py install
```

# How to use
Edit **twitch.py** in order to add some targets. The targets
are your recorded videos/streams where views will be generated
```python
targets = [
  "http://www.twitch.tv/kursion/b/465668354",
  "http://www.twitch.tv/kursion/b/465658800",
  "http://www.twitch.tv/kursion/b/465471324",
  "http://www.twitch.tv/kursion/b/458885660",
  "http://www.twitch.tv/kursion/b/458517856",
  "http://www.twitch.tv/kursion/b/458388622",
  "http://www.twitch.tv/kursion/b/414884311",
  "http://www.twitch.tv/kursion/c/2402962"
]
```

The views are generated randomly on those targets through
different proxies.
Also you can set the interval and the total number of views that you want
```python
nbr_request = 100
nbr_sleep_max = 30
```

