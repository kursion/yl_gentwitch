import re, random, time, urllib.request
from colorama import init, Fore, Back, Style
init()

username = "kursion"

# Total number of views/request on the targets
nbr_request = 100

# Time interval between request
sleep_min = 5
sleep_max = 15

targets = [
  "http://www.twitch.tv/kursion/b/465668354",
  "http://www.twitch.tv/kursion/b/465658800",
  "http://www.twitch.tv/kursion/b/465471324",
  "http://www.twitch.tv/kursion/b/458885660",
  "http://www.twitch.tv/kursion/b/458517856",
  "http://www.twitch.tv/kursion/b/458388622",
  "http://www.twitch.tv/kursion/b/414884311",
  "http://www.twitch.tv/kursion/c/2402962"
]

def getViewers():
  while (1):
    try:
      proxy = proxies[random.randint(0,len(proxies))-1]
      
      proxy_handler = urllib.request.ProxyHandler({'http': proxy})
      opener = urllib.request.build_opener(proxy_handler)
      f = opener.open("http://www.twitch.tv/"+username,timeout=10)
      page = str(f.read())+""
      html_string = ''.join( str(page) )
      matches = re.findall(r"title=\\'Channel Views\\'>(.*?)</span>", html_string)
      for m in matches:
        print(Fore.YELLOW+"\t\tGet viewers - Proxy: "+str(proxy)+" - "+Fore.GREEN+"[OK]"+Fore.RESET)
        return m
      return "-"
    except KeyboardInterrupt:
        raise
    except: print(Fore.YELLOW+"\t\tGet viewers - Proxy: "+str(proxy)+" - "+Fore.RED+"[FAILED]"+Fore.RESET)

# Get a list of proxies from free-proxy-list.net/
def getProxies():
  
  proxy_list_filtered = []
  
  f = urllib.request.urlopen("http://gatherproxy.com/", timeout=10)
  proxy_list = f.read()
  html_string = ''.join( str(proxy_list) )
  matches = re.findall(r'PROXY_COUNTRY(.*?)PROXY_REFS', html_string)
  for m in matches:
      ip = re.findall(r'\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b', m)
      for m2 in ip: ip = m2
      ip = str(ip)
      s = m.replace(ip, "")
      
      port = re.findall(r'PROXY_PORT\":\"\d{1,5}', s)
      for m3 in port: port = m3.replace("PROXY_PORT\":\"", ""); break
      port = str(port)
      
      if len(ip) > 3:
        addr = "http://"+ip+":"+port+"/"
        if addr != "[]:[]": proxy_list_filtered.append(addr)
      
  return proxy_list_filtered

# Create a view/request from a proxy to a target
def makeRequest(proxy, target, i):
    try:
      proxy_handler = urllib.request.ProxyHandler({'http': proxy})
      opener = urllib.request.build_opener(proxy_handler)
      f = opener.open(target)
      r = f.read()
      output = open("results/"+str(i)+".html", "wb")
      output.write(r)
      output.close()

      if (str(r)+"").find("banned") != -1 :
        return False
      return True
    except KeyboardInterrupt:
        raise
    except: 
        return False
     
# MAIN  
proxies = getProxies()
i = 0
iSuccess = 0
Style.BRIGHT

print(Back.CYAN+"--- Generator for Twitch ---"+Back.RESET)
print("\t\t By Kursion")
print("- username:\t"+username)
print("- Proxies:\t"+str(len(proxies)))
print("- Targets:\t"+str(len(targets)))
print(Back.CYAN+"----------------------------"+Back.RESET)
print("")

ra_refresh = random.randint(10, 40)
while (i < nbr_request):
  ra_proxy = proxies[random.randint(0,len(proxies))-1] # Random proxy
  ra_target = targets[random.randint(0,len(targets))-1] # Random target
  ra_time = random.randint(sleep_min, sleep_max) # Random sleep time
  
  if i%ra_refresh == 0: 
    print (Back.GREEN+"N°\t| Target \t\t\t\t\t| Proxy \t\t\t| Sleep"+Back.RESET)
    proxies = getProxies() # reloading the proxy list
    cached_viewers = getViewers();
    ra_refresh = random.randint(10, 20)
    print("#> Next refresh in "+str(ra_refresh))
  
  i += 1
  
  print(str(i)+">\t  "+Fore.YELLOW+ra_target+"\033[30m \t| \033[36m"+ra_proxy+"\033[30m \t| \033[35m"+str(ra_time)+"\033[30m")
  r = makeRequest(ra_proxy, ra_target, i)
  if(r == False): print("\t  "+Fore.BLUE+"Views:"+cached_viewers+Fore.RED+" | [Failed]"+Fore.RESET)
  else: t = Fore.GREEN+"[OK]"+Fore.RESET; iSuccess += 1
  
  if(r == True):
    print(str(i)+">\t  \033[35mSleeping for "+str(ra_time)+" | "+Fore.BLUE+" Views:"+cached_viewers+" "+t+Fore.RESET)
    time.sleep(ra_time)

print(Fore.MAGENTA+"Generation done with "+str(iSuccess)+" success requests | views: "+getViewers()+Fore.RESET)


  
